#include <cstdlib>
#include <cstdio>
#include <cmath>

#include <gl/freeglut.h>

#include "../Solver/Types.hpp"

#define GLUT_KEY_ESCAPE	(0x1b)

GLint width = 800, height = 600;

GLfloat areaSize = 400.0f;

vector_t data;
solver_t max;
int N = 0;

/// \brief ������������ ������ ��� ���������� ����������� ������� � ����������.
/// \param i ���������� �� ��� �.
/// \param j ���������� �� ��� Y.
/// \param k ���������� �� ��� Z.
/// \return ������ � ���������� �������.
inline int idx(int i, int j, int k)
{
	return i * N * N + j * N + k;
}

///
void DrawRect(float x1, float y1, float x2, float y2);

/// \brief ������� ��������� �����.
void Display();

/* ���������� ��� ��������� �������� ���� */
void Reshape(GLint w, GLint h);

/* ������������ ��������� �� ���� */
void Mouse(int button, int state, int x, int y);

/* ������������ ��������� �� ���������� */
void Keyboard(unsigned char key, int x, int y);
void SpecialKeyboard(int key, int x, int y);

/* ��������� ������ �� �����. */
void LoadData(const std::string &FileName, vector_t &Output);

int main(int argc, char **argv)
{
	LoadData("SolutionGPU.csv", data);

	N = (int)std::pow((float)data.size(), 1.0f / 3.0f);
	
	for each (solver_t val in data)
		if (val > max)
			max = val;

	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_RGB);
	glutInitWindowSize(width, height);
	glutCreateWindow("Visualizator");
	
	glutDisplayFunc(Display);
	glutReshapeFunc(Reshape);
	glutKeyboardFunc(Keyboard);
	glutSpecialFunc(SpecialKeyboard);
	glutMouseFunc(Mouse);

	glutMainLoop();	

	return EXIT_SUCCESS;
}

/// \brief ������� ��������� �����.
void Display()
{
	glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT);

	GLbyte colorGrid[] = { 0x66, 0x66, 0x66 };
	GLbyte colorAxisX[] = { 0xFF, 0x00, 0x00 };
	GLbyte colorAxisY[] = { 0x00, 0xFF, 0x00 };
	GLbyte colorAxisZ[] = { 0x00, 0x00, 0xFF };

	/* �����. */
	glColor3bv(colorGrid);

	for (int i = 0; i < N + 1; ++i)
	{
		GLfloat h = i * areaSize / N;
				
		glBegin(GL_LINES);
		{
			// oXY.
			glVertex3f(h, 0.0f, 0.0f);
			glVertex3f(h, areaSize, 0.0f);
			glVertex3f(0.0f, h, 0.0f);
			glVertex3f(areaSize, h, 0.0f);
			
			// oYZ.
			glVertex3f(0.0f, h, 0.0f);
			glVertex3f(0.0f, h,	areaSize);
			glVertex3f(0.0f, 0.0f, h);
			glVertex3f(0.0f, areaSize, h);

			// oXZ.
			glVertex3f(h, 0.0f, 0.0f);
			glVertex3f(h, 0.0f, areaSize);
			glVertex3f(0.0f, 0.0f, h);
			glVertex3f(areaSize, 0.0f, h);
		}												
		glEnd();
	}
	
	/* ���. */	
	glBegin(GL_LINES);
	{		
		// oX.
		glColor3bv(colorAxisX);
		glVertex3f(0.0f,		0.0f, 0.0f);
		glVertex3f(areaSize,	0.0f, 0.0f);
		// oY.
		glColor3bv(colorAxisY);
		glVertex3f(0.0f, 0.0f, 0.0f);
		glVertex3f(0.0f, areaSize, 0.0f);
		// oZ.
		glColor3bv(colorAxisZ);
		glVertex3f(0.0f, 0.0f, 0.0f);
		glVertex3f(0.0f, 0.0f, areaSize);
	}
	glEnd();

	for (int i = 0; i < N; ++i)
		for (int j = 0; j < N; ++j)
			for (int k = 0; k < N; ++k)
			{
				glPushMatrix();
					glTranslatef(i * areaSize / N, j * areaSize / N, k * areaSize / N);
					glColor3b(data[idx(i, j, k)] * 0xFF / max, 0x66 / max, data[idx(i, j, k)] * 0xFF / max);
					glutSolidSphere(10, 10, 10);
				glPopMatrix();
			}

	glutSwapBuffers();
}

/* ���������� ��� ��������� �������� ���� */
void Reshape(int w, int h)
{
	width = w;
	height = h;

	glViewport(0, 0, w, h);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	glOrtho(-w / 2 + areaSize / 2, w / 2 + areaSize / 2, -h / 2, h / 2, -w, w);
	
	glRotatef(-45, 1, 0, 0);
	glRotatef(-90 - 20, 0, 0, 1);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
}

/* ������������ ��������� �� ���� */
void Mouse(int button, int state, int x, int y)
{
	if (state == GLUT_DOWN)
	{
		switch (button)
		{
			case GLUT_LEFT_BUTTON:
				
			break;
			case GLUT_RIGHT_BUTTON:
				glutFullScreenToggle();
			break;
		}

		glutPostRedisplay();
	}
}

/* ������������ ��������� �� ���������� */
void Keyboard(unsigned char key, int x, int y)
{
	switch (key)
	{
	case GLUT_KEY_ESCAPE:
		glutExit();
	break;
	}
}

void SpecialKeyboard(int key, int x, int y)
{
	switch (key)
	{
	case GLUT_KEY_F11:
		glutFullScreenToggle();
	break;
	case GLUT_KEY_PAGE_DOWN:
		
	break;
	case GLUT_KEY_PAGE_UP:
		
	break;
	case GLUT_KEY_LEFT:
		
	break;
	case GLUT_KEY_RIGHT:
		
	break;
	case GLUT_KEY_UP:
		
	break;
	case GLUT_KEY_DOWN:
		
	break;
	}
}

void LoadData(const std::string &FileName, vector_t &Output)
{
	solver_t value = 0.0f;

	FILE *fData = fopen(FileName.c_str(), "r");

	if (fData)
	{
		while (!feof(fData))
		{
			fscanf(fData, "%f;", &value);

			if (!feof(fData))
				Output.push_back(value);
		}
		
		fclose(fData);
	}
}
