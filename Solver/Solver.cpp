#include "SolverApplication.hpp"

int main(int argc, char *argv[])
{
	return SolverApplication(argc, argv).Run();
}