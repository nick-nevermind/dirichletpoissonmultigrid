#pragma once

#include "Types.hpp"

class Grid
{
public:
	/// \brief ���������� �������� ��� ����� ���-�����������.
	static const int PRE_SMOOTH_COUNT = 50;
	/// \brief ���������� �������� ��� ����� ����-�����������.
	static const int POST_SMOOTH_COUNT = 50;

private:
	/* ������ � ������� �����. */
	int N, level;
	/* ��� �����. */
	solver_t h;

public:
	/// \brief �����������.
	/// \param Level ������� �����.
	Grid(int Level);

	/// \brief ���������� ������� �����.
	/// \return ������� �����.
	int GetLevel() const;

	/// \brief ���������� ������ �����.
	/// \return ������ �����.
	inline int GetSize() const;

	/// \brief ���������� ��� �����.
	/// \return ��� �����.
	inline solver_t GetStep() const;

	/// \brief ���������� ������ ������� ������.
	/// \return ������ ������� ������.
	inline int GetVectorSize() const;

	/// \brief ���������� �������� � � ���� �����.
	/// \param i ����� ����.
	/// \return �������� � � ���� �����.
	inline solver_t x(int i) const;

	/// \brief ���������� �������� Y � ���� �����.
	/// \param j ����� ����.
	/// \return �������� Y � ���� �����.
	inline solver_t y(int j) const;

	/// \brief ���������� �������� Z � ���� �����.
	/// \param k ����� ����.
	/// \return �������� Z � ���� �����.
	inline solver_t z(int k) const;

	/// \brief ������������ ������ ��� ���������� ����������� ������� � ����������.
	/// \param i ���������� �� ��� �.
	/// \param j ���������� �� ��� Y.
	/// \param k ���������� �� ��� Z.
	/// \return ������ � ���������� �������.
	inline int idx(int i, int j, int k) const;

	/// \brief ������������ ������ ��� ���������� ����������� ������� � ����������.
	/// \param i ���������� �� ��� �.
	/// \param j ���������� �� ��� Y.
	/// \return ������ � ���������� �������.
	inline int idx2(int i, int j) const;

	/// \brief �������-��������� ������ ����� � ��������� �������.
	/// \param i ���������� �� ��� �.
	/// \param j ���������� �� ��� Y.
	/// \param k ���������� �� ��� Z.
	/// \return �������� Fijk + Gijk.
	inline solver_t F(int i, int j, int k) const;

	/// \brief ���������� ������ ������ ����� � ��������� ������� � ����� ����.
	/// \remark ����� ��� ������� ������� ��-�� ������������� ������������ �� ������ ������, � ����� ��� ���������� �����������.
	/// \param Vector [out] ������ ������ ����� � ��������� �������.
	void GetVectorF(vector_t &Vector) const;

	/// \brief �������-��������� ������� ������������� ���� ��� ��������� ��������.
	/// \param i ���������� �� ��� �.
	/// \param j ���������� �� ��� Y.
	/// \param k ���������� �� ��� Z.
	/// \param n ����� �������� � ������ ��������������� ���� (i, j, k)
	/// \return ���������� n-� ������� ������ ��������������� ���� (i, j, k).
	solver_t A(int i, int j, int k, int n) const;

	/// \brief ���������� ������� ������������� ����.
	/// \remark ����� ��� ������� ������� ��-�� ������������� ������������ �� ������ ������.
	/// \param Matrix [out] ������� ������������� ����.
	void GetMatrixA(vector_t &Matrix) const;

	/// \brief �������-��������� ������� �������� �� ������ � ������ �����.
	/// \param i ������ � ������� ��������. 
	/// \param j ������� � ������� ��������.
	inline solver_t I(int i, int j) const;

	/// \brief ������� ������ � ������������ � ������� �����.
	/// \param Vector [in, out] ������ ��� ������.
	void Shrink(vector_t &Vector) const;

	/// \brief ��������� ������ � ������������ � ������� �����.
	/// \param Vector [in, out] ������ ��� ��������.
	void Unshrink(vector_t &Vector) const;

	/// \brief ������ ������� ���� ������� ������.
	/// \param a [in, out] ������� ������������� ����.
	/// \param b [in, out] ������ ������ �����.
	/// \param x [out] ������� ������.
	void Gauss(vector_t &a, vector_t &b, vector_t &x) const;
}; // class Grid