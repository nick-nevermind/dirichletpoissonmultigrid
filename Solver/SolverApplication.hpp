#pragma once

#include <cstdlib>
#include <cstdio>
#include <cassert>
#include <ctime>

#include <getopt.h>

#include "Log.hpp"
#include "CPUMultigridSolver.hpp"
#include "GPUMultigridSolver.hpp"

class SolverApplication
{
private:
	int argc;
	char **argv;

	bool onCpu, onGpu, isConsoleOutput;
	std::string cpuSolutionFile, gpuSolutionFile;	

	int levelBegin;

	CPUMultigridSolver cpuSolver;
	GPUMultigridSolver gpuSolver;

public:
	/// \brief �����������.
	/// \param argc ���������� ���������� ���������� ������.
	/// \param argv ������ ���������� ���������� ������.
	SolverApplication(int argc, char **argv);

	/// \brief ��������� ����������.
	int Run();

private:
	/// \brief �������������� ��������� ��������.
	void InitOptions();

	/// \brief ������� � ����������� ����� ������ ������� ������� �� ������������� ��������.
	void PrintUsage();
}; // class SolverApplication