#pragma once

#include "Types.hpp"
#include "Grid.hpp"
#include "Utility.hpp"
#include "Log.hpp"
#include "OpenCLHelper.hpp"

/// \brief ���������� �������� �� GPU.
class GPUMultigridSolver
{
private:
	OpenCLHelper ocl;

public:
	/// \brief ��������� W-���� �������������� ������.
	/// \brief ��������� ������� �����.
	vector_t Start(int LevelBegin);

private:
	/// \brief ���������� W-����� �������������� ������.
	/// \brief grid ������� �����.
	/// \brief U ����������� �������� �������.
	/// \brief Res [out] ������� ������.
	void WCycle(const Grid &grid, vector_t U, vector_t &Res);
}; // class GPUMultigridSolver