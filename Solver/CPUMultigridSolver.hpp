#pragma once

#include "Types.hpp"
#include "Grid.hpp"
#include "Utility.hpp"
#include "Log.hpp"

/// \brief ���������� �������� �� CPU.
class CPUMultigridSolver
{
public:
	/// \brief ��������� W-���� �������������� ������.
	/// \brief ��������� ������� �����.
	vector_t Start(int LevelBegin);

private:
	/// \brief ���������� W-����� �������������� ������.
	/// \brief grid ������� �����.
	/// \brief U ����������� �������� �������.
	/// \brief Res [out] ������� ������.
	void WCycle(const Grid &grid, vector_t U, vector_t &Res);
}; // class CPUMultigridSolver