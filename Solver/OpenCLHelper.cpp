#include "OpenCLHelper.hpp"

/// \brief �����������.
OpenCLHelper::OpenCLHelper() :
	platform(NULL), device(NULL), context(NULL), commandQueue(NULL), program(NULL),
	ckSmooth(NULL), ckResidualCorrection(NULL), ckShrink(NULL),
	devU(NULL), devR(NULL)
{ }

void OpenCLHelper::InitOCL()
{		
	std::string source;
	size_t programLength;
	const size_t bufSize = 256;
	char buf[bufSize];

	Log::Info("OpenCL initialization...");
		
	// ��������� ���������.
	lastError = clGetPlatformIDs(1, &platform, NULL);
	CheckError();
	lastError = clGetPlatformInfo(platform, CL_PLATFORM_VERSION, bufSize * sizeof(buf[0]), buf, NULL);
	CheckError();
	Log::Info("Platform version: %s", buf);

	// ��������� ����������.
	lastError = clGetDeviceIDs(platform, CL_DEVICE_TYPE_GPU, 1, &device, NULL);
	CheckError();
	lastError = clGetDeviceInfo(device, CL_DEVICE_NAME, bufSize * sizeof(buf[0]), buf, NULL);
	CheckError();
	Log::Info("Device name: %s", buf);

	// �������� ��������� � ������� �������.
	Log::Info("Creating Context...");
	context = clCreateContext(NULL, 1, &device, NULL, NULL, &lastError);
	CheckError();
	Log::Info("Creating Command Queue...");
	commandQueue = clCreateCommandQueue(context, device, NULL, &lastError);
	CheckError();

	// ������������� ������������.
	Log::Info("Building Program...");
	
	source = Utility::LoadFile("Kernels.c");
	
	if (!source.empty())
	{
		programLength = source.size();
		const char *szSource = &source[0];
		program = clCreateProgramWithSource(context, 1, &szSource, &programLength, &lastError);
		CheckError();
		lastError = clBuildProgram(program, 0, NULL, NULL, NULL, NULL);
		CheckError();

		// ������������� ����.
		Log::Info("Creating kernels...");
		ckSmooth = clCreateKernel(program, "Smooth", &lastError);
		ckResidualCorrection = clCreateKernel(program, "ResidualCorrection", &lastError);
		ckShrink = clCreateKernel(program, "Shrink", &lastError);
		ckUnshrink = clCreateKernel(program, "Unshrink", &lastError);
		CheckError();
	}
	else Log::Error("Cannot load Kernels.c or its empty.");
}

void OpenCLHelper::Smooth(vector_t &U, vector_t &R, int N)
{
	assert(U.size() == R.size() && U.size() == N * N * N);

	// ���������� ������.
	devU = clCreateBuffer(context, CL_MEM_READ_WRITE, sizeof(solver_t) * U.size(), NULL, &lastError), CheckError();
	devR = clCreateBuffer(context, CL_MEM_READ_WRITE, sizeof(solver_t) * R.size(), NULL, &lastError), CheckError();

	// �������� ���������� ����.
	lastError = clSetKernelArg(ckSmooth, 0, sizeof(cl_mem), (void *)&devU), CheckError();
	lastError = clSetKernelArg(ckSmooth, 1, sizeof(cl_mem), (void *)&devR), CheckError();
	lastError = clSetKernelArg(ckSmooth, 2, sizeof(cl_int), (void *)&N), CheckError();
	
	lastError = clEnqueueWriteBuffer(commandQueue, devU, false, 0, sizeof(solver_t) * U.size(), (void *)U.data(), 0, NULL, NULL), CheckError();	
	lastError = clEnqueueWriteBuffer(commandQueue, devR, false, 0, sizeof(solver_t) * R.size(), (void *)R.data(), 0, NULL, NULL), CheckError();

	size_t globalSize[] = { N, N, N };

	// ������ ����.
	lastError = clEnqueueNDRangeKernel(commandQueue, ckSmooth, 3, NULL, globalSize, NULL, 0, NULL, NULL), CheckError();

	// ��������� ����������.
	lastError = clEnqueueReadBuffer(commandQueue, devU, true, 0, sizeof(solver_t) * U.size(), (void *)U.data(), 0, NULL, NULL), CheckError();
	lastError = clEnqueueReadBuffer(commandQueue, devR, true, 0, sizeof(solver_t) * R.size(), (void *)R.data(), 0, NULL, NULL), CheckError();

	// ������������ ��������.
	if (devU) { clReleaseMemObject(devU); devU = NULL; }
	if (devR) { clReleaseMemObject(devR); devR = NULL; }
}

/// \brief ��������� �������.
/// \param U ������� �������.
/// \param R �������.
/// \param N ������ ������.
void OpenCLHelper::ResidualCorrection(vector_t &U, vector_t &R, int N)
{
	assert(U.size() == R.size() && U.size() == N * N * N);

	// ���������� ������.
	devU = clCreateBuffer(context, CL_MEM_READ_ONLY, sizeof(solver_t) * U.size(), NULL, &lastError), CheckError();
	devR = clCreateBuffer(context, CL_MEM_READ_WRITE, sizeof(solver_t) * R.size(), NULL, &lastError), CheckError();

	// �������� ���������� ����.
	lastError = clSetKernelArg(ckResidualCorrection, 0, sizeof(cl_mem), (void *)&devU), CheckError();
	lastError = clSetKernelArg(ckResidualCorrection, 1, sizeof(cl_mem), (void *)&devR), CheckError();
	lastError = clSetKernelArg(ckResidualCorrection, 2, sizeof(cl_int), (void *)&N), CheckError();
	
	lastError = clEnqueueWriteBuffer(commandQueue, devU, false, 0, sizeof(solver_t) * U.size(), (void *)U.data(), 0, NULL, NULL), CheckError();	
	lastError = clEnqueueWriteBuffer(commandQueue, devR, false, 0, sizeof(solver_t) * R.size(), (void *)R.data(), 0, NULL, NULL), CheckError();

	size_t globalSize[] = { N, N, N };

	// ������ ����.
	lastError = clEnqueueNDRangeKernel(commandQueue, ckResidualCorrection, 3, NULL, globalSize, NULL, 0, NULL, NULL), CheckError();

	// ��������� ����������.
	lastError = clEnqueueReadBuffer(commandQueue, devR, true, 0, sizeof(solver_t) * R.size(), (void *)R.data(), 0, NULL, NULL), CheckError();

	// ������������ ��������.
	if (devU) { clReleaseMemObject(devU); devU = NULL; }
	if (devR) { clReleaseMemObject(devR); devR = NULL; }
}

/// \brief ������� ������ � ������������ � ������� �����.
/// \param Vector [in, out] ������ ��� ������.
/// \param N ������ ������.
void OpenCLHelper::Shrink(vector_t &Vector)
{
	vector_t S(Vector.size() / 8); // pow(2.0, dimentions);
		
	// ���������� ������.
	devU = clCreateBuffer(context, CL_MEM_READ_ONLY, sizeof(solver_t) * Vector.size(), NULL, &lastError), CheckError();
	devR = clCreateBuffer(context, CL_MEM_READ_WRITE, sizeof(solver_t) * S.size(), NULL, &lastError), CheckError();

	// �������� ���������� ����.
	lastError = clSetKernelArg(ckShrink, 0, sizeof(cl_mem), (void *)&devU), CheckError();
	lastError = clSetKernelArg(ckShrink, 1, sizeof(cl_mem), (void *)&devR), CheckError();
	
	lastError = clEnqueueWriteBuffer(commandQueue, devU, false, 0, sizeof(solver_t) * Vector.size(), (void *)Vector.data(), 0, NULL, NULL), CheckError();	
	lastError = clEnqueueWriteBuffer(commandQueue, devR, false, 0, sizeof(solver_t) * S.size(), (void *)S.data(), 0, NULL, NULL), CheckError();

	size_t globalSize[] = { S.size(), Vector.size() };

	// ������ ����.
	lastError = clEnqueueNDRangeKernel(commandQueue, ckShrink, 2, NULL, globalSize, NULL, 0, NULL, NULL), CheckError();

	// ��������� ����������.
	lastError = clEnqueueReadBuffer(commandQueue, devR, true, 0, sizeof(solver_t) * S.size(), (void *)S.data(), 0, NULL, NULL), CheckError();

	// ������������ ��������.
	if (devU) { clReleaseMemObject(devU); devU = NULL; }
	if (devR) { clReleaseMemObject(devR); devR = NULL; }

	Vector = S;
}

/// \brief ��������� ������ � ������������ � ������� �����.
/// \param Vector [in, out] ������ ��� ��������.
/// \param N ������ ������.
void OpenCLHelper::Unshrink(vector_t &Vector)
{
	vector_t S(Vector.size() * 8); // pow(2.0, dimentions);

	// ���������� ������.
	devU = clCreateBuffer(context, CL_MEM_READ_ONLY, sizeof(solver_t) * Vector.size(), NULL, &lastError), CheckError();
	devR = clCreateBuffer(context, CL_MEM_READ_WRITE, sizeof(solver_t) * S.size(), NULL, &lastError), CheckError();

	// �������� ���������� ����.
	lastError = clSetKernelArg(ckShrink, 0, sizeof(cl_mem), (void *)&devU), CheckError();
	lastError = clSetKernelArg(ckShrink, 1, sizeof(cl_mem), (void *)&devR), CheckError();
	
	lastError = clEnqueueWriteBuffer(commandQueue, devU, false, 0, sizeof(solver_t) * Vector.size(), (void *)Vector.data(), 0, NULL, NULL), CheckError();	
	lastError = clEnqueueWriteBuffer(commandQueue, devR, false, 0, sizeof(solver_t) * S.size(), (void *)S.data(), 0, NULL, NULL), CheckError();

	size_t globalSize[] = { S.size(), Vector.size() };

	// ������ ����.
	lastError = clEnqueueNDRangeKernel(commandQueue, ckShrink, 2, NULL, globalSize, NULL, 0, NULL, NULL), CheckError();

	// ��������� ����������.
	lastError = clEnqueueReadBuffer(commandQueue, devR, true, 0, sizeof(solver_t) * S.size(), (void *)S.data(), 0, NULL, NULL), CheckError();

	// ������������ ��������.
	if (devU) { clReleaseMemObject(devU); devU = NULL; }
	if (devR) { clReleaseMemObject(devR); devR = NULL; }

	Vector = S;
}

void OpenCLHelper::Cleanup()
{
	if (ckSmooth)		{ clReleaseKernel(ckSmooth); ckSmooth = NULL; }
	if (program)		{ clReleaseProgram(program); program = NULL; }
	if (commandQueue)	{ clReleaseCommandQueue(commandQueue); commandQueue = NULL; }
	if (context)		{ clReleaseContext(context); context = NULL; }
	if (devU)			{ clReleaseMemObject(devU); devU = NULL; }
	if (devR)			{ clReleaseMemObject(devR); devR = NULL; }
}

void OpenCLHelper::CheckError()
{
	if (lastError != CL_SUCCESS)
	{
		Log::Error("OpenCL Engine Error: %s", GetErrorString(lastError).c_str());
		Cleanup();
		exit(lastError);
	}
}

/// \brief ���������� ����� ������ OpenCL.
/// \param err ��� ������.
/// \return ����� ������.
std::string OpenCLHelper::GetErrorString(const cl_int &err)
{
	std::string s;

	switch (err)
	{
		case CL_SUCCESS: s = "CL_SUCCESS"; break;
		case CL_DEVICE_NOT_FOUND: s = "CL_DEVICE_NOT_FOUND"; break;
		case CL_DEVICE_NOT_AVAILABLE: s = "CL_DEVICE_NOT_AVAILABLE"; break;
		case CL_COMPILER_NOT_AVAILABLE: s = "CL_COMPILER_NOT_AVAILABLE"; break;
		case CL_MEM_OBJECT_ALLOCATION_FAILURE: s = "CL_MEM_OBJECT_ALLOCATION_FAILURE"; break;
		case CL_OUT_OF_RESOURCES: s = "CL_OUT_OF_RESOURCES"; break;
		case CL_OUT_OF_HOST_MEMORY: s = "CL_OUT_OF_HOST_MEMORY"; break;
		case CL_PROFILING_INFO_NOT_AVAILABLE: s = "CL_PROFILING_INFO_NOT_AVAILABLE"; break;
		case CL_MEM_COPY_OVERLAP: s = "CL_MEM_COPY_OVERLAP"; break;
		case CL_IMAGE_FORMAT_MISMATCH: s = "CL_IMAGE_FORMAT_MISMATCH"; break;
		case CL_IMAGE_FORMAT_NOT_SUPPORTED: s = "CL_IMAGE_FORMAT_NOT_SUPPORTED"; break;
		case CL_BUILD_PROGRAM_FAILURE: s = "CL_BUILD_PROGRAM_FAILURE"; break;
		case CL_MAP_FAILURE: s = "CL_MAP_FAILURE"; break;
		case CL_MISALIGNED_SUB_BUFFER_OFFSET: s = "CL_MISALIGNED_SUB_BUFFER_OFFSET"; break;
		case CL_EXEC_STATUS_ERROR_FOR_EVENTS_IN_WAIT_LIST: s = "CL_EXEC_STATUS_ERROR_FOR_EVENTS_IN_WAIT_LIST"; break;
		case CL_INVALID_VALUE: s = "CL_INVALID_VALUE"; break;
		case CL_INVALID_DEVICE_TYPE: s = "CL_INVALID_DEVICE_TYPE"; break;
		case CL_INVALID_PLATFORM: s = "CL_INVALID_PLATFORM"; break;
		case CL_INVALID_DEVICE: s = "CL_INVALID_DEVICE"; break;
		case CL_INVALID_CONTEXT: s = "CL_INVALID_CONTEXT"; break;
		case CL_INVALID_QUEUE_PROPERTIES: s = "CL_INVALID_QUEUE_PROPERTIES"; break;
		case CL_INVALID_COMMAND_QUEUE: s = "CL_INVALID_COMMAND_QUEUE"; break;
		case CL_INVALID_HOST_PTR: s = "CL_INVALID_HOST_PTR"; break;
		case CL_INVALID_MEM_OBJECT: s = "CL_INVALID_MEM_OBJECT"; break;
		case CL_INVALID_IMAGE_FORMAT_DESCRIPTOR: s = "CL_INVALID_IMAGE_FORMAT_DESCRIPTOR"; break;
		case CL_INVALID_IMAGE_SIZE: s = "CL_INVALID_IMAGE_SIZE"; break;
		case CL_INVALID_SAMPLER: s = "CL_INVALID_SAMPLER"; break;
		case CL_INVALID_BINARY: s = "CL_INVALID_BINARY"; break;
		case CL_INVALID_BUILD_OPTIONS: s = "CL_INVALID_BUILD_OPTIONS"; break;
		case CL_INVALID_PROGRAM: s = "CL_INVALID_PROGRAM"; break;
		case CL_INVALID_PROGRAM_EXECUTABLE: s = "CL_INVALID_PROGRAM_EXECUTABLE"; break;
		case CL_INVALID_KERNEL_NAME: s = "CL_INVALID_KERNEL_NAME"; break;
		case CL_INVALID_KERNEL_DEFINITION: s = "CL_INVALID_KERNEL_DEFINITION"; break;
		case CL_INVALID_KERNEL: s = "CL_INVALID_KERNEL"; break;
		case CL_INVALID_ARG_INDEX: s = "CL_INVALID_ARG_INDEX"; break;
		case CL_INVALID_ARG_VALUE: s = "CL_INVALID_ARG_VALUE"; break;
		case CL_INVALID_ARG_SIZE: s = "CL_INVALID_ARG_SIZE"; break;
		case CL_INVALID_KERNEL_ARGS: s = "CL_INVALID_KERNEL_ARGS"; break;
		case CL_INVALID_WORK_DIMENSION: s = "CL_INVALID_WORK_DIMENSION"; break;
		case CL_INVALID_WORK_GROUP_SIZE: s = "CL_INVALID_WORK_GROUP_SIZE"; break;
		case CL_INVALID_WORK_ITEM_SIZE: s = "CL_INVALID_WORK_ITEM_SIZE"; break;
		case CL_INVALID_GLOBAL_OFFSET: s = "CL_INVALID_GLOBAL_OFFSET"; break;
		case CL_INVALID_EVENT_WAIT_LIST: s = "CL_INVALID_EVENT_WAIT_LIST"; break;
		case CL_INVALID_EVENT: s = "CL_INVALID_EVENT"; break;
		case CL_INVALID_OPERATION: s = "CL_INVALID_OPERATION"; break;
		case CL_INVALID_GL_OBJECT: s = "CL_INVALID_GL_OBJECT"; break;
		case CL_INVALID_BUFFER_SIZE: s = "CL_INVALID_BUFFER_SIZE"; break;
		case CL_INVALID_MIP_LEVEL: s = "CL_INVALID_MIP_LEVEL"; break;
		case CL_INVALID_GLOBAL_WORK_SIZE: s = "CL_INVALID_GLOBAL_WORK_SIZE"; break;
		case CL_INVALID_PROPERTY: s = "CL_INVALID_PROPERTY"; break;
	}

	return s;
}