#define solver_t float

/// \brief ������������ ������ ��� ���������� ����������� ������� � ����������.
/// \param N ����������� ������.
/// \param i ���������� �� ��� �.
/// \param j ���������� �� ��� Y.
/// \param k ���������� �� ��� Z.
/// \return ������ � ���������� �������.
int idx(int N, int i, int j, int k)
{
	return i * N * N + j * N + k;
}

/// \brief ���������� ������ ������� ������.
/// \param N ����������� ������.
/// \return ������ ������� ������.
int GetVectorSize(int N)
{ 
	return N * N * N; 
}

/// \brief �������-��������� ������ ����� � ��������� �������.
/// \param N ����������� ������.
/// \param i ���������� �� ��� �.
/// \param j ���������� �� ��� Y.
/// \param k ���������� �� ��� Z.
/// \return �������� Fijk + Gijk.
solver_t F(int N, int i, int j, int k)
{
	solver_t res = 0.0f;
	solver_t h = 1.0f / N;

	/* ������� ������ �����. */
	//res = x(i) * y(j) * z(k);
	res = 0; // ��������� �������.

	/* ��������� �������. */
	if		( i == 0	 ) res -= 0;//y(j) - z(k);
	else if ( i == N - 1 ) res -= 0;							 
	else if ( j == 0	 ) res += i * h +  k * h;
	else if ( j == N - 1 ) res += i * h +  k * h;//x(i) * z(k);						 
	else if ( i == 0	 ) res += 0;
	else if ( i == N - 1 ) res += 0;

	return res;
}

/// \brief �������-��������� ������� ������������� ���� ��� ��������� ��������.
/// \param N ����������� ������.
/// \param i ���������� �� ��� �.
/// \param j ���������� �� ��� Y.
/// \param k ���������� �� ��� Z.
/// \param n ����� �������� � ������ ��������������� ���� (i, j, k)
/// \return ���������� n-� ������� ������ ��������������� ���� (i, j, k).
solver_t A(int N, int i, int j, int k, int n)
{
	solver_t r = 0;

	if (idx(N, i, j, k) == n)
		r = 1;
	else if ( i < N - 1	&& idx(N, i + 1,	j,		k	  ) == n ||
			  i > 0		&& idx(N, i - 1,	j,		k	  ) == n ||
			  j < N - 1	&& idx(N, i,		j + 1,	k	  ) == n ||
			  j > 0		&& idx(N, i,		j - 1,	k	  ) == n ||
			  k < N - 1	&& idx(N, i,		j,		k + 1 ) == n ||
			  k > 0		&& idx(N, i,		j,		k - 1 ) == n )
	{
		r = -1.0f / 6.0f;
	}

	return r;
}

/// \brief �������-��������� ������� �������� �� ������ � ������ �����.
/// \param i ������ � ������� ��������. 
/// \param j ������� � ������� ��������.
solver_t I(int i, int j)
{
	return (i / 8 == j) ? 1.0f / 8.0f : 0.0f;
}

/// \brief ������� ���������.
/// \param U ������� �������.
/// \param R �������.
/// \param N ������ ������.
__kernel void Smooth(__global solver_t *U, __global solver_t *R, int N)
{
	int i = get_global_id(0);
	int j = get_global_id(1);
	int k = get_global_id(2);

	solver_t sum = 0;

	for (int n = 0; n < GetVectorSize(N); ++n)
		sum += A(N, i, j, k, n) * U[n];

	sum -= F(N, i, j, k);
	R[idx(N, i, j, k)] = sum;
	U[idx(N, i, j, k)] -= sum * (2.0f / 3.0f);
}

/// \brief ��������� �������.
/// \param U ������� �������.
/// \param R �������.
/// \param N ������ ������.
__kernel void ResidualCorrection(__global solver_t *U, __global solver_t *R, int N)
{
	int i = get_global_id(0);
	int j = get_global_id(1);
	int k = get_global_id(2);

	solver_t sum = 0;

	for (int n = 0; n < GetVectorSize(N); ++n)
		sum += A(N, i, j, k, n) * U[n];

	R[idx(N, i, j, k)] = sum;
}

/// \brief ������� ������ � ������������ � ������� �����.
/// \param U ������ ��� ������.
/// \param U ��������� ������.
__kernel void Shrink(__global solver_t *U, __global solver_t *V)
{
	int i = get_global_id(0);
	int j = get_global_id(1);

	V[i] += I(j, i) * U[j];
}

/// \brief ��������� ������ � ������������ � ������� �����.
/// \param U ������ ��� ��������.
/// \param U ��������� ��������.
__kernel void Unshrink(__global solver_t *U, __global solver_t *V)
{
	int i = get_global_id(0);
	int j = get_global_id(1);

	V[i] += I(i, j) * U[j];
}