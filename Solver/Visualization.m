function [] = Visualization(FileNames)
    fileCount = length(FileNames);
    
    figure;
    
    for c = 1 : fileCount
        % �������� ����������.
        subplot(1, fileCount, c);
        
        % ������ ������.
        try
            data = importdata(cell2mat(FileNames(c)));
            
            if (~isempty(data))
                n = length(data);
                n = nthroot(n, 3);        

                % ���������� � ���������� ������.
                U = zeros(n, n, n);
                for i = 1 : n
                    for j = 1 : n
                        for k = 1 : n
                            U(i, j, k) = data((i - 1) * n ^ 2 + (j - 1) * n + k);
                        end
                    end        
                end

                % ���������� �������.
                p1 = floor(n / 3) + 1;
                p2 = floor(n * 2 / 3) + 1;

                % ���������� �������.
                slice(U, [p1, p2], [p1, p2], [p1, p2]);

                % ������� ���� � ������.
                %colormap(hot);
                xlim([0, n + 2]);
                ylim([0, n + 2]);
                zlim([0, n + 2]);
                camproj('perspective');
                grid off;
            end        
        catch err
            if (strcmp(err.identifier, 'MATLAB:importdata:FileNotFound'))            
                msg = sprintf('���� "%s" �� ������.', cell2mat(FileNames(c)));
                axis off;
                text(0, .5, msg);
            else
                rethrow(err);
            end
        end       
    end
    
    set(gcf, 'Color', 'white', 'Renderer', 'zbuffer');
    
    clearvars;
end

