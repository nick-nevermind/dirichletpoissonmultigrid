#pragma once

#include <cstdlib>
#include <cstdio>
#include <string>
#include <sstream>
#include <iomanip>
#include <ctime>
#include <vadefs.h>
#include <Windows.h>

/// \brief ���.
/*static*/ class Log
{
public:
	enum LogMessageType
	{
		T_INFO,
		T_WARNING,
		T_ERROR
	};

private:
	CRITICAL_SECTION cSection;
	std::string fileName;
	bool isFileOutput;	

private:
	/// \brief �����������.
	Log();

	/// \brief ����������.
	~Log();

	/// \brief ����������� �����������.
	Log(const Log &);

	/// \brief �������� ����������.
	Log & operator = (const Log &);

private:
	/// \brief ��������� ������ � ������� ����� � ��������.
	/// \return ������ � ������� ����� � ��������.
	static std::string GetTime();

	/// \brief ���������� ������ � ����� ���������.
	/// \retun ������ � ����� ���������.
	static std::string GetTypeText(const LogMessageType &Type);

	/// \brief ����������� ������ � ������� ����.
	/// \param Data ������ ��� ����������.
	void SaveFile(const std::string &Data);
	
	/// \brief ���������� ��������� � ���.
	/// \param Type ��� ���������.
	/// \param Msg ����� ���������.
	/// \param args ��������� ���������.
	void Message(const LogMessageType &Type, std::string Msg, va_list args);

	/// \brief ���������� ��������� ����.
	static Log & Instance();

public:
	/// \brief ������������� ����� ���� � ����.
	/// \param FileName ��� �����.
	static void SetFileOutput(const std::string &FileName);

	/// \brief ������������� ����� ���� � stderr.
	static void SetConsoleOutput();

	/// \brief ���������� ��������� � ���.
	/// \param Type ��� ���������.
	/// \param Msg ����� ���������.
	/// \param ... ��������� ���������.
	static void Message(const LogMessageType &Type, std::string Msg, ...);	

	/// \brief ���������� �������������� ��������� � ���.
	/// \param Msg ����� ���������.
	/// \param ... ��������� ���������.
	static void Info(std::string Msg, ...);	

	/// \brief ���������� ���������-�������������� � ���.
	/// \param Msg ����� ���������.
	/// \param ... ��������� ���������.
	static void Warning(std::string Msg, ...);	

	/// \brief ���������� ��������� �� ������ � ���.
	/// \param Msg ����� ���������.
	/// \param ... ��������� ���������.
	static void Error(std::string Msg, ...);
}; // class Log