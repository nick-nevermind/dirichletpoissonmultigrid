#include "GPUMultigridSolver.hpp"

/// \brief ��������� W-���� �������������� ������.
/// \brief ��������� ������� �����.
vector_t GPUMultigridSolver::Start(int LevelBegin)
{
	vector_t U0, U;
	Grid grid(LevelBegin);
	
	// ��������� �����������.
	grid.GetVectorF(U0);

	ocl.InitOCL();	

	WCycle(grid, U0, U);

	ocl.Cleanup();

	return U;
}

void GPUMultigridSolver::WCycle(const Grid &grid, vector_t U, vector_t &Res)
{
	if (grid.GetLevel() == 1)
	{
		/* ������ �������. ������� �� CPU. */
		Log::Info("Level %d> Direct solving... ", grid.GetLevel());
		int N = grid.GetSize() * grid.GetSize() - 1;

		// ������� �������������, ������ ���� �.� ������ ����� 64 ����� � ����� ������ ������������ �� ������ ������.
		vector_t a, f;
		grid.GetMatrixA(a);
		grid.GetVectorF(f);
		grid.Gauss(a, f, U);

		Res = U;
		Log::Info("Finished");
	}
	else
	{
	/* ������ �����. */
		Grid shortGrid(grid.GetLevel() - 1);			

		vector_t R(grid.GetVectorSize()), E;

		/* ���-�����������. */
		Log::Info("Level %d> Pre-smooth... ", grid.GetLevel());
		for (int l = 0; l < Grid::PRE_SMOOTH_COUNT; ++l)
		{
			R.clear(); R.resize(grid.GetVectorSize());
			ocl.Smooth(U, R, grid.GetSize());
		}
		Log::Info("Finished");

		/* ������� ������ ������� � �������. */
		Log::Info("Level %d> Shrinking... ", grid.GetLevel());
		/* R = I * R; U = I * U; */
		ocl.Shrink(U);
		ocl.Shrink(R);
		Log::Info("Finished");

		/* ������ ����������� �����. */
		WCycle(shortGrid, R, E);

		/* ������������� �� ������ �����. */
		Log::Info("Level %d> Correction on small grid... ", grid.GetLevel());
		/* U -= E */
		for (int l = 0; l < shortGrid.GetVectorSize(); ++l)
			U[l] -= E[l];
		Log::Info("Finished");

		/* �������� �������. */
		Log::Info("Level %d> Residual Correction on small grid... ", grid.GetLevel());
		R.clear(); R.resize(shortGrid.GetVectorSize());
		ocl.ResidualCorrection(U, R, shortGrid.GetSize());
		Log::Info("Finished");

		WCycle(shortGrid, R, E);

		/* ��������� ���������. */
		Log::Info("Level %d> UnShrinking... ", grid.GetLevel());
		ocl.Unshrink(U);
		ocl.Unshrink(E);
		Log::Info("Finished");

		/* ������������� �� ������� �����. */
		Log::Info("Level %d> Residual Correction on big grid... ", grid.GetLevel());
		/* U -= E */
		for (int l = 0; l < grid.GetVectorSize(); ++l)
			U[l] -= E[l];
		Log::Info("Finished");

		/* ����-�����������. */
		Log::Info("Level %d> Post-smooth... ", grid.GetLevel());
		for (int l = 0; l < Grid::POST_SMOOTH_COUNT; ++l)
		{
			R.clear(); R.resize(grid.GetVectorSize());
			ocl.Smooth(U, R, grid.GetSize());
		}
		Log::Info("Finished");

		Res = U;
	}
}