#include "SolverApplication.hpp"

/// \brief �����������.
/// \param argc ���������� ���������� ���������� ������.
/// \param argv ������ ���������� ���������� ������.
SolverApplication::SolverApplication(int argc, char **argv) :
	argc(argc), argv(argv), 
	onCpu(false), onGpu(false), 
	cpuSolutionFile("SolutionCPU.csv"), gpuSolutionFile("SolutionGPU.csv"),
	levelBegin(3),
	isConsoleOutput(false)
{
	/* ������������� �����. */
	InitOptions();

	/* ������������� ����. */
	if (isConsoleOutput)
		Log::SetConsoleOutput();
	else Log::SetFileOutput("Solver.log");

	/* ���� �����. */
	Log::Info("Solver Application v1.0\n\nOptions:\nonCpu = %s\nonGpu = %s\nlevelBegin = %d\n", onCpu ? "true" : "false", onGpu ? "true" : "false", levelBegin);
}

/// \brief ��������� ����������.
int SolverApplication::Run()
{
	vector_t U;

	if (onCpu)
	{
		clock_t timeStart = clock();
		U = cpuSolver.Start(levelBegin);
		Log::Info("CPU Solver execution time: %.02fsec", ((double)(clock() - timeStart) / CLOCKS_PER_SEC));

		Utility::DumpVector(cpuSolutionFile, U);
	}

	if (onGpu)
	{
		clock_t timeStart = clock();
		U = gpuSolver.Start(levelBegin);
		Log::Info("GPU Solver execution time: %.02fsec", ((double)(clock() - timeStart) / CLOCKS_PER_SEC));

		Utility::DumpVector(gpuSolutionFile, U);
	}	

	return EXIT_SUCCESS;
}

/// \brief �������������� ��������� ��������.
void SolverApplication::InitOptions()
{
	struct option params[] =
	{
		{ "help",			no_argument,		NULL, 'h' },
		{ "gpu",			no_argument,		NULL, 'g' },
		{ "cpu",			no_argument,		NULL, 'c' },
		{ "console",		no_argument,		NULL, 'o' },
		{ "level",			required_argument,	NULL, 'l' },
		{ "pre-smooth",		required_argument,	NULL, 'p' },
		{ "post-smooth",	required_argument,	NULL, 's' },
		{ NULL, NULL, NULL, NULL }
	};

	char cmd = 0;
	int optionIndex = 0;
	extern char *optarg;

	while (cmd != -1)
	{
		switch (cmd = getopt_long(argc, argv, "hgcl:o", params, &optionIndex))
		{
		case 'h':
			PrintUsage();
			exit(EXIT_SUCCESS);
		break;
		case 'c':
			onCpu = true;
		break;
		case 'g':
			onGpu = true;
		break;
		case 'l':
			levelBegin = atoi(optarg);
		break;
		case 'o':
			isConsoleOutput = true;
		break;
		}
	}
}

/// \brief ������� � ����������� ����� ������ ������� ������� �� ������������� ��������.
void SolverApplication::PrintUsage()
{

}