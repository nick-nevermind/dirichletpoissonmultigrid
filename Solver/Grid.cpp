#include "Grid.hpp"

/// \brief �����������.
/// \param Level ������� �����.
Grid::Grid(int Level) :
	level(Level), N((int)std::pow(2.0, Level)), h(solver_t(1) / N)
{ }

/// \brief ���������� ������� �����.
/// \return ������� �����.
int Grid::GetLevel() const 
{ 
	return level; 
}

/// \brief ���������� ������ �����.
/// \return ������ �����.
inline int Grid::GetSize() const 
{ 
	return N; 
}

/// \brief ���������� ��� �����.
/// \return ��� �����.
inline solver_t Grid::GetStep() const 
{ 
	return h; 
}

/// \brief ���������� ������ ������� ������.
/// \return ������ ������� ������.
inline int Grid::GetVectorSize() const
{ 
	return N * N * N; 
}

/// \brief ���������� �������� � � ���� �����.
/// \param i ����� ����.
/// \return �������� � � ���� �����.
inline solver_t Grid::x(int i) const
{ 
	return i * h; 
}

/// \brief ���������� �������� Y � ���� �����.
/// \param j ����� ����.
/// \return �������� Y � ���� �����.
inline solver_t Grid::y(int j) const 
{
	return j * h; 
}

/// \brief ���������� �������� Z � ���� �����.
/// \param k ����� ����.
/// \return �������� Z � ���� �����.
inline solver_t Grid::z(int k) const
{ 
	return k * h; 
}

/// \brief ������������ ������ ��� ���������� ����������� ������� � ����������.
/// \param i ���������� �� ��� �.
/// \param j ���������� �� ��� Y.
/// \param k ���������� �� ��� Z.
/// \return ������ � ���������� �������.
inline int Grid::idx(int i, int j, int k) const
{
	return i * N * N + j * N + k;
}

/// \brief ������������ ������ ��� ���������� ����������� ������� � ����������.
/// \param i ���������� �� ��� �.
/// \param j ���������� �� ��� Y.
/// \return ������ � ���������� �������.
inline int Grid::idx2(int i, int j) const
{
	return i * GetVectorSize() + j;
}

/// \brief �������-��������� ������ ����� � ��������� �������.
/// \param i ���������� �� ��� �.
/// \param j ���������� �� ��� Y.
/// \param k ���������� �� ��� Z.
/// \return �������� Fijk + Gijk.
inline solver_t Grid::F(int i, int j, int k) const
{
	solver_t res = 0.0f;
		
	/* ������� ������ �����. */
	//res = x(i) * y(j) * z(k);
	res = 0; // ��������� �������.

	/* ��������� �������. */
	if		( i == 0	 ) res -= 0;//y(j) - z(k);
	else if ( i == N - 1 ) res -= 0;							 
	else if ( j == 0	 ) res += x(i) + z(k);
	else if ( j == N - 1 ) res += x(i) + z(k);						 
	else if ( i == 0	 ) res += 0;
	else if ( i == N - 1 ) res += 0;

	return res;
}

/// \brief ���������� ������ ������ ����� � ��������� ������� � ����� ����.
/// \remark ����� ��� ������� ������� ��-�� ������������� ������������ �� ������ ������, � ����� ��� ���������� �����������.
/// \param Vector [out] ������ ������ ����� � ��������� �������.
void Grid::GetVectorF(vector_t &Vector) const
{
	Vector.clear();

	for (int i = 0; i < GetSize(); ++i)
		for (int j = 0; j < GetSize(); ++j)
			for (int k = 0; k < GetSize(); ++k)
				Vector.push_back(F(i, j, k));
}

/// \brief �������-��������� ������� ������������� ���� ��� ��������� ��������.
/// \param i ���������� �� ��� �.
/// \param j ���������� �� ��� Y.
/// \param k ���������� �� ��� Z.
/// \param n ����� �������� � ������ ��������������� ���� (i, j, k)
/// \return ���������� n-� ������� ������ ��������������� ���� (i, j, k).
solver_t Grid::A(int i, int j, int k, int n) const
{
	solver_t r = 0;

	if (idx(i, j, k) == n)
		r = 1;
	else if ( i < N - 1	&& idx( i + 1,	j,		k	  ) == n ||
			  i > 0		&& idx( i - 1,	j,		k	  ) == n ||
			  j < N - 1	&& idx( i,		j + 1,	k	  ) == n ||
			  j > 0		&& idx( i,		j - 1,	k	  ) == n ||
			  k < N - 1	&& idx( i,		j,		k + 1 ) == n ||
			  k > 0		&& idx( i,		j,		k - 1 ) == n )
	{
		r = -1.0f / 6.0f;
	}

	return r;
}

/// \brief ���������� ������� ������������� ����.
/// \remark ����� ��� ������� ������� ��-�� ������������� ������������ �� ������ ������.
/// \param Matrix [out] ������� ������������� ����.
void Grid::GetMatrixA(vector_t &Matrix) const
{
	Matrix.clear();

	for (int i = 0; i < GetSize(); ++i)
		for (int j = 0; j < GetSize(); ++j)
			for (int k = 0; k < GetSize(); ++k)
				for (int n = 0; n < GetVectorSize(); ++n)
					Matrix.push_back(A(i, j, k, n));
}

/// \brief �������-��������� ������� �������� �� ������ � ������ �����.
/// \param i ������ � ������� ��������. 
/// \param j ������� � ������� ��������.
inline solver_t Grid::I(int i, int j) const
{
	return (i / 8 == j) ? 1.0f / 8.0f : 0.0f;
}

/// \brief ������� ������ � ������������ � ������� �����.
/// \param Vector [in, out] ������ ��� ������.
void Grid::Shrink(vector_t &Vector) const
{
	vector_t S(Vector.size() / 8); // pow(2.0, dimentions);

	for (int i = 0; i < S.size(); ++i)
		for (int j = 0; j < Vector.size(); j++)
			S[i] += I(j, i) * Vector[j];

	Vector = S;
}

/// \brief ��������� ������ � ������������ � ������� �����.
/// \param Vector [in, out] ������ ��� ��������.
void Grid::Unshrink(vector_t &Vector) const
{
	vector_t S(Vector.size() * 8); // pow(2.0, dimentions);

	for (int i = 0; i < S.size(); ++i)
		for (int j = 0; j < Vector.size(); j++)
			S[i] += I(i, j) * Vector[j];

	Vector = S;
}

/// \brief ������ ������� ���� ������� ������.
/// \param a [in, out] ������� ������������� ����.
/// \param b [in, out] ������ ������ �����.
/// \param x [out] ������� ������.
void Grid::Gauss(vector_t &a, vector_t &b, vector_t &x) const
{
	solver_t v = 0.0;

	for (int k = 0, i, j, im; k < GetVectorSize() - 1; ++k)
	{
		im = k;

		for (i = k + 1; i < GetVectorSize(); ++i)
		{
			if (fabs(a[idx2(im, k)]) < fabs(a[idx2(i, k)]))
				im = i;
		}

		if (im != k)
		{
			for (j = 0; j < GetVectorSize(); ++j)
			{
				v = a[idx2(im, j)];
				a[idx2(im, j)] = a[idx2(k, j)];
				a[idx2(k, j)]  = v;
			}

			v = b[im];
			b[im] = b[k];
			b[k] = v;
		}

		for (i = k + 1; i < GetVectorSize(); ++i)
		{
			v = a[idx2(i, k)] / a[idx2(k, k)];
			a[idx2(i, k)] = 0;
			b[i] = b[i] - v * b[k];
			
			if (v != 0)
				for (j = k + 1; j < GetVectorSize(); ++j)
					a[idx2(i, j)] -= v * a[idx2(k, j)];				
		}
	}

	solver_t s = 0;
	
	x[GetVectorSize() - 1] = b[GetVectorSize() - 1] / a[idx2(GetVectorSize() - 1, GetVectorSize() - 1)];

	for (int i = GetVectorSize() - 2, j; 0 <= i; --i)
	{
		s = 0;

		for (j = i + 1; j < GetVectorSize(); ++j)
			s = s + a[idx2(i, j)] * x[j];

		x[i] = (b[i] - s) / a[idx2(i, i)];
	}	
}