#pragma once

#include <cassert>
#include <CL/opencl.h>

#include "Types.hpp"
#include "Log.hpp"
#include "Utility.hpp"

class OpenCLHelper
{
	private:
		cl_int lastError;
		cl_platform_id platform;
		cl_device_id device;
		cl_context context;
		cl_command_queue commandQueue;
		cl_program program;
		cl_kernel ckSmooth, ckResidualCorrection, ckShrink, ckUnshrink;
		cl_mem devU, devR;

	public:
		/// \brief �����������.
		OpenCLHelper();

		/// \brief ������� ���������.
		/// \param U ������� �������.
		/// \param R �������.
		/// \param N ������ ������.
		void Smooth(vector_t &U, vector_t &R, int N);

		/// \brief ��������� �������.
		/// \param U ������� �������.
		/// \param R �������.
		/// \param N ������ ������.
		void ResidualCorrection(vector_t &U, vector_t &R, int N);

		/// \brief ������� ������ � ������������ � ������� �����.
		/// \param Vector [in, out] ������ ��� ������.
		void Shrink(vector_t &Vector);

		/// \brief ��������� ������ � ������������ � ������� �����.
		/// \param Vector [in, out] ������ ��� ��������.
		void Unshrink(vector_t &Vector);

		/// \brief ������������� OpenCL.
		void InitOCL();
		
		/// \brief ������� ��������.
		void Cleanup();
		
		/// \brief ���������� ������.
		void CheckError();		
		
		/// \brief ���������� ����� ������ OpenCL.
		/// \param err ��� ������.
		/// \return ����� ������.
		std::string GetErrorString(const cl_int &err);
}; // class OpenCLHelper