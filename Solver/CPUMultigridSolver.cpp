#include "CPUMultigridSolver.hpp"

/// \brief ��������� W-���� �������������� ������.
/// \brief ��������� ������� �����.
vector_t CPUMultigridSolver::Start(int LevelBegin)
{
	vector_t U0, U;
	Grid grid(LevelBegin);
	
	// ��������� �����������.
	grid.GetVectorF(U0);

	WCycle(grid, U0, U);

	return U;
}

/// \brief ���������� W-����� �������������� ������.
/// \brief grid ������� �����.
/// \brief U ����������� �������� �������.
/// \brief Res [out] ������� ������.
void CPUMultigridSolver::WCycle(const Grid &grid, vector_t U, vector_t &Res)
{		
	if (grid.GetLevel() == 1)
	{
		/* ������ �������. */
		Log::Info("Level %d> Direct solving... ", grid.GetLevel());
		int N = grid.GetSize() * grid.GetSize() - 1;

		// ������� �������������, ������ ���� �.� ������ ����� 64 ����� � ����� ������ ������������ �� ������ ������.
		vector_t a, f;
		grid.GetMatrixA(a);
		grid.GetVectorF(f);
		grid.Gauss(a, f, U);

		Res = U;
		Log::Info("Finished");
	}
	else
	{
		/* ������ �����. */
		Grid shortGrid(grid.GetLevel() - 1);			

		vector_t R(grid.GetVectorSize()), E;

		/* ���-�����������. */
		Log::Info("Level %d> Pre-smooth... ", grid.GetLevel());
		for (int l = 0; l < Grid::PRE_SMOOTH_COUNT; ++l)
		{
			/* R = A * U - F; */
			/* U = U - R * (2.0 / 3.0) */

			R.clear(); R.resize(grid.GetVectorSize());

			for (int i = 0; i < grid.GetSize(); ++i)
				for (int j = 0; j < grid.GetSize(); ++j)
					for (int k = 0; k < grid.GetSize(); ++k)
					{						
						for (int n = 0; n < grid.GetVectorSize(); ++n)							
							R[grid.idx(i, j, k)] += grid.A(i, j, k, n) * U[n];
						R[grid.idx(i, j, k)] -= grid.F(i, j, k);
						U[grid.idx(i, j, k)] -= R[grid.idx(i, j, k)] * (2.0f / 3.0f);
					}
		}
		Log::Info("Finished");

		/* ������� ������ ������� � �������. */
		Log::Info("Level %d> Shrinking... ", grid.GetLevel());
		/* R = I * R; U = I * U; */
		grid.Shrink(U);
		grid.Shrink(R);
		Log::Info("Finished");

		/* ������ ����������� �����. */
		WCycle(shortGrid, R, E);

		/* ������������� �� ������ �����. */
		Log::Info("Level %d> Correction on small grid... ", grid.GetLevel());
		/* U -= E */
		for (int l = 0; l < shortGrid.GetVectorSize(); ++l)
			U[l] -= E[l];
		Log::Info("Finished");

		/* �������� �������. */
		Log::Info("Level %d> Residual Correction on small grid... ", grid.GetLevel());
		R.clear(); R.resize(shortGrid.GetVectorSize());

		for (int i = 0; i < shortGrid.GetSize(); ++i)
			for (int j = 0; j < shortGrid.GetSize(); ++j)
				for (int k = 0; k < shortGrid.GetSize(); ++k)
				{						
					for (int n = 0; n < shortGrid.GetVectorSize(); ++n)							
						R[shortGrid.idx(i, j, k)] += shortGrid.A(i, j, k, n) * U[n];
					R[shortGrid.idx(i, j, k)] -= shortGrid.F(i, j, k);
				}
		Log::Info("Finished");

		WCycle(shortGrid, R, E);

		/* ��������� ���������. */
		Log::Info("Level %d> UnShrinking... ", grid.GetLevel());
		shortGrid.Unshrink(U);
		shortGrid.Unshrink(E);
		Log::Info("Finished");

		/* ������������� �� ������� �����. */
		Log::Info("Level %d> Residual Correction on big grid... ", grid.GetLevel());
		/* U -= E */
		for (int l = 0; l < grid.GetVectorSize(); ++l)
			U[l] -= E[l];
		Log::Info("Finished");

		/* ����-�����������. */
		Log::Info("Level %d> Post-smooth... ", grid.GetLevel());
		for (int l = 0; l < Grid::POST_SMOOTH_COUNT; ++l)
		{
			/* R = A * U - F; */
			/* U = U - R * (2.0 / 3.0) */

			R.clear(); R.resize(grid.GetVectorSize());

			for (int i = 0; i < grid.GetSize(); ++i)
				for (int j = 0; j < grid.GetSize(); ++j)
					for (int k = 0; k < grid.GetSize(); ++k)
					{						
						for (int n = 0; n < grid.GetVectorSize(); ++n)							
							R[grid.idx(i, j, k)] += grid.A(i, j, k, n) * U[n];
						R[grid.idx(i, j, k)] -= grid.F(i, j, k);
						U[grid.idx(i, j, k)] -= R[grid.idx(i, j, k)] * (2.0f / 3.0f);
					}
		}
		Log::Info("Finished");

		Res = U;
	}
}