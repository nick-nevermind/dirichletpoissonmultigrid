#include "Utility.hpp"

#define FILE_READ_BUFFER (2 * 1024)

/// \brief ������� ������ � �����.
/// \warning ���������� ����������� ��� Visual C++ �������� "for each".
/// \param Output ���������� ������.
/// \param Vector ������ ��� ������.
void Utility::DumpVector(FILE *Output, const vector_t &Vector)
{
	for each (solver_t e in Vector)
		fprintf(Output, "%f;", e);
}

/// \brief ������� ������ � ����.
/// \param FileName ��� �����.
/// \param Vector ������ ��� ������.
void Utility::DumpVector(const std::string &FileName, const vector_t &Vector)
{
	FILE *hFile = fopen(FileName.c_str(), "w");

	if (hFile)
	{
		DumpVector(hFile, Vector);
		fclose(hFile);
	}
}

/// \brief ������� �������� �����.
/// \param path ���� � �����.
/// \return ���������� �����.
std::string Utility::LoadFile(const std::string &path)
{	
	std::string rc;
	char szBuffer[FILE_READ_BUFFER] = "\0";

	HANDLE hFile = CreateFileA(path.c_str(), GENERIC_READ, FILE_SHARE_READ | FILE_SHARE_DELETE | FILE_SHARE_WRITE, 0, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, 0);
	DWORD nReaded = 0ul, nReadedTotal = 0ul;

	if (hFile != INVALID_HANDLE_VALUE)
	{			
		do
		{
			ReadFile(hFile, szBuffer, FILE_READ_BUFFER - 1, &nReaded, NULL);
			szBuffer[nReaded] = '\0';
			rc += szBuffer;
		}
		while (nReaded);

		FlushFileBuffers(hFile);
		CloseHandle(hFile);
		rc.shrink_to_fit();
	}

	return rc;
}