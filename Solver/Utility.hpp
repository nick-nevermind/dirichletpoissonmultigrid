#pragma once

#include <cstdio>
#include <Windows.h>

#include "Types.hpp"

/// \brief ����������� ������.
/*static*/ class Utility
{
protected:
	/* ������ ����������������. */
	Utility();
	Utility(const Utility &);
	Utility & operator = (const Utility &);

public:
	/// \brief ������� ������ � �����.
	/// \warning ���������� ����������� ��� Visual C++ �������� "for each".
	/// \param Output ���������� ������.
	/// \param Vector ������ ��� ������.
	static void DumpVector(FILE *Output, const vector_t &Vector);

	/// \brief ������� ������ � ����.
	/// \param FileName ��� �����.
	/// \param Vector ������ ��� ������.
	static void DumpVector(const std::string &FileName, const vector_t &Vector);

	/// \brief ������� �������� �����.
	/// \param path ���� � �����.
	/// \return ���������� �����.
	static std::string LoadFile(const std::string &path);
}; // class Utility