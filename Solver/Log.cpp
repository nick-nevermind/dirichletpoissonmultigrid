#include "Log.hpp"

/// \brief �����������.
Log::Log() :
	isFileOutput(false)
{ 
	::InitializeCriticalSection(&cSection);
}

/// \brief ����������.
Log::~Log()
{
	::DeleteCriticalSection(&cSection);
}

/// \brief ��������� ������ � ������� ����� � ��������.
/// \return ������ � ������� ����� � ��������.
std::string Log::GetTime()
{
	char szDataTimeString[20] = "";
	time_t rawtime = 0;
	
	time(&rawtime);	
	struct tm *info = localtime(&rawtime);

	sprintf_s(szDataTimeString, 20, "%02d.%02d.%04d %02d:%02d:%02d", info->tm_mday,
																	 info->tm_mon + 1,
																	 info->tm_year + 1900,
																	 info->tm_hour,
																	 info->tm_min,
																	 info->tm_sec );
	return std::string(szDataTimeString);
}

/// \brief ���������� ������ � ����� ���������.
/// \retun ������ � ����� ���������.
std::string Log::GetTypeText(const LogMessageType &Type)
{
	std::string text;

	switch (Type)
	{
	case T_INFO:
		text = "[Info]";
	break;
	case T_WARNING:
		text = "[Warning]";
	break;
	case T_ERROR:
		text = "[Error]";
	break;
	}

	return text;
}

/// \brief ����������� ������ � ������� ����.
/// \param Data ������ ��� ����������.
void Log::SaveFile(const std::string &Data)
{
	HANDLE hFile = ::CreateFileA(fileName.c_str(), GENERIC_WRITE, FILE_SHARE_READ | FILE_SHARE_DELETE, NULL, OPEN_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);

	if (hFile != INVALID_HANDLE_VALUE)
	{
		DWORD ulWrited = 0ul;

		::SetFilePointer(hFile, 0, NULL, FILE_END);
		::WriteFile(hFile, Data.c_str(), (DWORD)strlen(Data.c_str()), &ulWrited, NULL);
		::WriteFile(hFile, "\r\n", 1, &ulWrited, NULL);
		::FlushFileBuffers(hFile);
		::CloseHandle(hFile);
	}
}

/// \brief ���������� ��������� � ���.
/// \param Type ��� ���������.
/// \param Msg ����� ���������.
/// \param args ��������� ���������.
void Log::Message(const LogMessageType &Type, std::string Msg, va_list args)
{
	std::stringstream buf;
	
	::EnterCriticalSection(&cSection);
	{
		std::string formattedMsg;
		formattedMsg.resize(256, '\0');

		int len = vsnprintf_s(&formattedMsg[0], formattedMsg.size(), _TRUNCATE, Msg.c_str(), args);

		buf << GetTypeText(Type) << " " << GetTime() << " " << formattedMsg;

		if (isFileOutput)
			SaveFile(buf.str());
		else fprintf(stderr, "%s\r\n", buf.str().c_str());
	}
	::LeaveCriticalSection(&cSection);
}

/// \brief ���������� ��������� ����.
Log & Log::Instance()
{
	static Log instance;		
	return instance;
}

/// \brief ������������� ����� ���� � ����.
/// \param FileName ��� �����.
void Log::SetFileOutput(const std::string &FileName)
{
	Instance().fileName = FileName;
	Instance().isFileOutput = true;
}

/// \brief ������������� ����� ���� � stderr.
void Log::SetConsoleOutput()
{
	Instance().fileName = "";
	Instance().isFileOutput = false;
}

/// \brief ���������� ��������� � ���.
/// \param Type ��� ���������.
/// \param Msg ����� ���������.
/// \param ... ��������� ���������.
void Log::Message(const LogMessageType &Type, std::string Msg, ...)
{		
	va_list argptr;
	va_start(argptr, Msg);
	Instance().Message(Type, Msg, argptr);
	va_end(argptr);
}

/// \brief ���������� �������������� ��������� � ���.
/// \param Msg ����� ���������.
/// \param ... ��������� ���������.
void Log::Info(std::string Msg, ...)
{
	va_list argptr;
	va_start(argptr, Msg);
	Instance().Message(T_INFO, Msg, argptr);
	va_end(argptr);
}

/// \brief ���������� ���������-�������������� � ���.
/// \param Msg ����� ���������.
/// \param ... ��������� ���������.
void Log::Warning(std::string Msg, ...)
{
	va_list argptr;
	va_start(argptr, Msg);
	Instance().Message(T_WARNING, Msg, argptr);
	va_end(argptr);
}

/// \brief ���������� ��������� �� ������ � ���.
/// \param Msg ����� ���������.
/// \param ... ��������� ���������.
void Log::Error(std::string Msg, ...)
{
	va_list argptr;
	va_start(argptr, Msg);
	Instance().Message(T_ERROR, Msg, argptr);
	va_end(argptr);
}