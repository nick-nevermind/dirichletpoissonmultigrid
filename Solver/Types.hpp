#pragma once

#include <vector>

typedef float solver_t;
typedef std::vector<solver_t> vector_t;